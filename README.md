# TTree

Alows to roll out a twitter thread and display it in console

## Usage

```
ttree <id or URL of the first tweet of the thread>
```

## Options

- `--md`: format output as Markdown
- `--nodetails`: don’t fetch detail of each status (prevent tree display);
- `--noreply`: don’t fetch every reply, only main thread
- `--notree`: flat display;
- `--dump`: dump result as JSON;

## Sample

Script that takes a tweet id or URL, grab the thread, output it as markdown, then use marked to create an HTML page
```
output="$(basename $1).html"
echo "<style> @import url(’style.css’); </style>" > $output
ttree --noreply --md $1 | marked >> $output
echo "https://…/$(basename $output)"
```

## Note

This tool doesn’t use Twitter API because of:
 - there’s no API to easily retrieve a full thread;
 - request rate are way too low.

So, this is just a bun of hacks to scrap the Web client.

## License

GPL V3, what else, because, you know, “There is no system but GNU, and Linux is one of its kernels”.
