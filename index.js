#!/usr/bin/env nodejs
/*jshint node: true, laxbreak: true, maxstatements: 100, maxcomplexity: 100 */
var request = require('request'),
    cheerio = require('cheerio'),
    $, tweets = [], promises = [],
    options;

options = {
  details: true,
  dump: false,
  md: false,
  reply: true,
  tree: true
};
process.on('uncaughtException', function (err) {
  "use strict";
  console.error(err);
  console.error(err.stack);
});

// Handle command arguments
require('./args.js').parseArgv(options);
if (!options.details || !options.reply) {
  options.tree = false;
}

function fetchTweet(tweet, resolve) {
  "use strict";
  var requestOptions = {
    url: tweet.url,
    gzip: true,
    headers: {
      'Accept': 'application/json, text/javascript, */*; q=0.01',
      'X-Overlay-Request': 'true'
    }
  };
  request(requestOptions, function (error, response, body) {
    var $$;
    if (error) {
      console.error("Error fetch " + tweet.url);
      console.error(error);
      resolve(tweet);
      return;
    }
    if (response.statusCode !== 200) {
      console.error("Status code: ", response.statusCode);
      console.log("Error fetching ", tweet.url);
      resolve(tweet);
      return;
    }
    try {
      //tweet.full = JSON.parse(body.toString()).page;
      $$ = cheerio.load(JSON.parse(body.toString()).page);
    } catch (e) {
      console.error("Error parsing response", body.toString());
      console.error(e);
      resolve(tweet);
      return;
    }
    tweet.url = "https://twitter.com" + $$('.permalink-tweet-container [data-permalink-path]').attr("data-permalink-path");
    tweet.replied = $$("[data-replied-tweet-id]").attr("data-replied-tweet-id");
    if (!tweet.text) {
      // Remove hidden links
      $$('.twitter-timeline-link.u-hidden').text('');
      tweet.text = $$('.permalink-tweet .tweet-text').text();
    }
    if (!tweet.displayName) {
      tweet.displayName = $$('.permalink-tweet .fullname').text();
    }
    if (!tweet.screenName) {
      tweet.screenName = $$('.permalink-tweet .username').text();
    }
    if (!tweet.date) {
      tweet.date = new Date(parseInt($$('.permalink-tweet [data-time-ms]').attr("data-time-ms")));
    }
    tweet.medias = $$(".permalink-tweet-container [data-image-url]").map(function (i, e) { return $$(e).attr("data-image-url");}).get();
    resolve(tweet);
  });
}
function fetch(url, cb) {
  "use strict";
  request(url, function (error, response, body) {
    var res;
    if (error) {
      console.error(error);
      return;
    }
    if (response.statusCode !== 200) {
      console.error("Status code: ", response.statusCode);
      console.log("Error fetching ", url);
      return;
    }
    try {
      res = JSON.parse(body);
    } catch (e) {
      console.error(e);
      return;
    }
    //console.log(res.html);
    $ = cheerio.load(res.html);
    $('.Timeline-item').each(function (i, tweet) {
      var item;
      tweet = $(tweet);
      // Remove hidden links
      $('.twitter-timeline-link.u-hidden').text('');
      item = {
        displayName: tweet.find('.UserNames-displayName').text(),
        screenName: tweet.find('.UserNames-screenName').text(),
        text: tweet.find('.Tweet-text').text(),
        url: tweet.find('.Tweet-timestamp').attr('href').replace('mobile.', '')
      };
      item.id = item.url.split('/').pop();
      if ( options.reply
        || ( item.screenName === tweets[0].screenName && item.text[0] !== '@')) {
        tweets.push(item);
      }
    });
    if (options.details) {
      tweets.forEach(function (tweet) {
        var p = new Promise(function (resolve, reject) {
          fetchTweet(tweet, resolve);
        });
        promises.push(p);
      });
    }
    Promise.all(promises).then(function (values) {
      var all = {};
      function display(i, tab) {
        console.log(tab + ' - ' + all[i].displaytext);
        all[i].replies.forEach(function (rep) {
          display(rep.id, tab + '  ');
        });
      }
      if (res.nextCursorEndpoint) {
        fetch('https://mobile.twitter.com' + res.nextCursorEndpoint);
      } else {
        tweets.forEach(function (tweet) {
          tweet.displaytext = tweet.text.replace("\n", " ", 'g');
          if (options.md) {
            tweet.medias.forEach( function (media) {
              tweet.displaytext += ' ![' + media + '](' + media + ')';
            });
            tweet.displaytext += " — [" + tweet.date.toISOString() + "](" + tweet.url + ") ";
          }
        });
        if (!options.dump) {
          if (options.md) {
            console.log('# [' + tweets[0].text + '](' + tweets[0].url + ')');
          } else {
            console.log('# ' + tweets[0].url);
          }
        }
        if (options.dump) {
          console.log(tweets);
        } else if (options.tree) {
          tweets.forEach(function (tweet) {
            all[tweet.id] = tweet;
            tweet.displaytext = '[' + tweet.displayName + ' - ' + tweet.screenName + '] ' + tweet.displaytext;
            tweet.replies = [];
            if (tweet.replied && all[tweet.replied]) {
              all[tweet.replied].replies.push(tweet);
            }
          });
          display(options._param, '');
          if (options.dump) {
            console.log(all, tweets);
          }
        } else {
          tweets.forEach(function (tweet) {
            if (options.reply) {
              tweet.displaytext = ' - [' + tweet.displayName + ' - ' + tweet.screenName + '] ' + tweet.displaytext;
            } else {
              tweet.displaytext = ' - ' + tweet.displaytext;
            }
            console.log(tweet.displaytext);
          });
        }
      }
    }).catch(function (e) {
      console.log(e);
    });

  });
}
if (options._param) {
  if (/^https/.test(options._param)) {
    options._param = options._param.split('/').pop();
  }
  fetchTweet({id: options._param, url: 'https://twitter.com/xxxx/status/' + options._param}, function (tweet) {
    "use strict";
    tweets.push(tweet);
    fetch('https://mobile.twitter.com/i/rw/conversation/' + options._param + '/' + options._param + '/descendants/' + options._param, function (error, res) {});
  });
} else {
  console.log("Usage: " + process.argv[0] + ' ' + process.argv[1] + ' tweetId');
}
