/*jshint node: true, laxbreak: true */
var exports = module.exports = {};
exports.parseArgv = function (options) {
  "use strict";
  if (typeof options === 'undefined') {
    options = {};
  }
  function usage() {
    console.log("usage: " + process.argv[1] + " [--nodetails|--notree|--noreply|--dump] id");
    process.exit(0);
  }

  var args = process.argv.slice(2),
      def;
  if (args.length === 0) {
    usage();
  } else {
    args.forEach(function (arg) {
      def = true;
      if (arg.substr(0, 2) === '--') {
        arg = arg.substr(2);
        if (  typeof options[arg] === 'undefined'
           && arg.substr(0, 2) === 'no'
           && typeof options[arg.substr(2)] !== 'undefined') {
          arg = arg.substr(2);
          def = false;
        }
        if (typeof options[arg] === 'undefined') {
          console.log(`Unknown argument --${arg}`);
          usage();
        } else {
          arg = arg.split('=');
          if (arg.length === 1) {
            options[arg[0]] = def;
          } else {
            options[arg[0]] = arg[1];
          }
        }
      } else {
        options._param = arg;
      }
    });
  }
};

